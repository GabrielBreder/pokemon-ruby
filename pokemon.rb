require 'json'

pokemons_api = File.read('./pokemons.json')
pokemons = JSON.parse(pokemons_api)

sum = 0
names = []
now_name = nil

pokemons_results = pokemons['results']
pokemons_results.each {|pokemon| 
  pokemon_name = pokemon['name'].split('-')[0]
  names.append(pokemon_name)
}

names = names.sort
names.each {|name|
  if name != now_name
    sum += 1
    now_name = name
  end
}

puts "Existem #{sum} pokemons distintos no arquivo."

tally_names = names.tally 

puts "\nOS 10 pokémons com maior occorrência são: \n"
tally_names = tally_names.sort_by {|name, occurrence| occurrence}

for i in 1..10 do
  puts tally_names[-i][0]
end

first_letters_count = []
count_letters = 0
now_letter = 'a'

names.each {|name| 
  if name[0] != now_letter
    first_letters_count.append([count_letters, name[0]])
    count_letters = 0

    now_letter = name[0]
    count_letters += 1
  else
    count_letters += 1
  end
}

first_letters_count = first_letters_count.sort.reverse

puts "\nAs letras iniciais mais frequentes são: \n"

for i in 0..4 do
  puts first_letters_count[i][1]
end